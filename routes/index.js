const router = require('express').Router();
require('express-group-routes');
const gameRoutes = require('./game.router');
const gameHistoryRoutes = require('./userGameHistory.router');
const authenticationRoutes = require('./authentication.router');
const userProfileRoutes = require('./userProfile.router');
const photoMediaRoutes = require('./photoMedia.router');
const videoMediaRoutes = require('./videoMedia.router');

const bodyParser = require('body-parser');

const jsonParser = bodyParser.json();

router.use('/api', jsonParser, gameRoutes);
router.use('/api', jsonParser, gameHistoryRoutes);
router.use('/api', jsonParser, userProfileRoutes);
router.use('/api', jsonParser, authenticationRoutes);
router.use('/api', photoMediaRoutes);
router.use('/api', videoMediaRoutes);

module.exports = router;
