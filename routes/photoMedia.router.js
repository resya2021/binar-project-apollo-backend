const express = require('express');
const multer = require('multer');
const { index, create, updates, deletes } = require('../controllers/photo.controller');
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();
const photoMediaRoutes = express.Router();

const storage = multer.diskStorage({
  // notice you are calling the multer.diskStorage() method here, not multer()
  destination: function (req, file, cb) {
    cb(null, 'public/photo');
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + '.png');
  },
});
const upload = multer({ storage }); //provide the return value from

photoMediaRoutes.get('/photoMedia/:id', index);
photoMediaRoutes.post('/photoMedia/:id', upload.single('photo'), create);
photoMediaRoutes.put('/photoMedia/update/:id', upload.single('photo'), updates);
photoMediaRoutes.delete('/photoMedia/delete/:id', deletes);

module.exports = photoMediaRoutes;
