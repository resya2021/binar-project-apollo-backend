const express = require('express');

const { index, create, updates, deletes } = require('../controllers/video.controller');
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();
const videoMediaRoutes = express.Router();

const multer = require('multer');
const storage = multer.diskStorage({
  // notice you are calling the multer.diskStorage() method here, not multer()
  destination: function (req, file, cb) {
    cb(null, 'public/video');
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + '.mp4');
  },
});
const upload = multer({ storage }); //provide the return value from

videoMediaRoutes.get('/videoMedia/:id', index);
videoMediaRoutes.post('/videoMedia/:id', upload.single('video'), create);
videoMediaRoutes.put('/videoMedia/update/:id', upload.single('video'), updates);
videoMediaRoutes.delete('/videoMedia/delete/:id', deletes);

module.exports = videoMediaRoutes;
