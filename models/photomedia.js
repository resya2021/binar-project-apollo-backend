'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PhotoMedia extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      PhotoMedia.belongsTo(models.user, {
        foreignKey: 'user_id',
        onDelete: 'CASCADE',
      });
    }
  }
  PhotoMedia.init(
    {
      name: DataTypes.STRING,
      originalName: DataTypes.STRING,
      description: DataTypes.STRING,
      photoPath: DataTypes.STRING,
      size: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: 'PhotoMedia',
    }
  );
  return PhotoMedia;
};
