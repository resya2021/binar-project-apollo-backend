const fs = require('fs');
const { PhotoMedia } = require('../models');
const { error, success } = require('../utils/response/json.utils');

module.exports = {
  index: async (req, res) => {
    const userId = req.params.id;
    try {
      const photos = await PhotoMedia.findAll({ where: { user_id: userId }, order: [['id', 'ASC']] });

      if (photos) {
        return success(res, 201, 'Success Get Data', { photos });
      } else return error(res, 401, 'Error', 'NULL');
    } catch (Error) {
      return error(res, 401, Error.message, 'Server Error');
    }
  },
  show: async (req, res) => {
    const IdPhoto = req.params.id;
    const photoData = await user_game_history.findOne({
      where: { id: IdPhoto },
    });

    try {
      if (photoData) {
        return success(res, 200, 'Success Get Data', { photoData });
      } else {
        return error(res, 400, 'Photo not found', 'NULL');
      }
    } catch (Error) {
      return error(res, 400, Error.message, 'Server Error');
    }
  },
  create: async (req, res) => {
    try {
      const { filename, originalname, path, size } = req.file;
      const { name, description } = req.body;
      const userId = req.params.id;

      const entry = await PhotoMedia.create({
        name: filename,
        originalName: originalname,
        description: description,
        photoPath: path,
        size: size,
        user_id: userId,
      });

      return success(res, 201, 'Data Photo Create Successfully', entry);
    } catch (Error) {
      return error(res, 401, Error.message, 'Server Error');
    }
  },
  updates: async (req, res) => {
    const { filename, originalname, path, size } = req.file;
    const { name, description } = req.body;
    const paramsId = req.params.id;
    const directoryPath = __basedir + '/public/photo/';

    try {
      const dataPhotoId = await PhotoMedia.findOne({
        where: { id: paramsId },
      });

      if (dataPhotoId) {
        const OldFileName = dataPhotoId.name;
        fs.unlink(directoryPath + OldFileName, (err) => {
          if (err) {
            res.status(500).send({
              message: 'Could not delete the file. ' + err,
            });
          }
        });
        let newData = {
          name: filename,
          originalName: originalname,
          description: description,
          photoPath: path,
          size: size,
        };
        const dataPhoto = await PhotoMedia.update(newData, {
          where: { id: paramsId },
        });
        return success(res, 200, 'Data Changed Successfully', newData);
      } else {
        return error(res, 401, 'Data not found');
      }
    } catch (Error) {
      return error(res, 400, Error.message, 'Server Error');
    }
  },
  deletes: async (req, res) => {
    const paramsId = req.params.id;
    const directoryPath = __basedir + '/public/photo/';
    try {
      const dataPhotoId = await PhotoMedia.findOne({
        where: { id: paramsId },
      });

      if (dataPhotoId) {
        const OldFileName = dataPhotoId.name;
        fs.unlink(directoryPath + OldFileName, (err) => {
          if (err) {
            res.status(500).send({
              message: 'Could not delete the file. ' + err,
            });
          }
        });
        let erase = await PhotoMedia.destroy({ where: { id: paramsId } });
        return success(res, 200, 'Data Deleted Successfully');
      } else {
        return error(res, 401, 'Data not found');
      }
    } catch (Error) {
      return error(res, 400, Error.message, 'Server Error');
    }
  },
};
