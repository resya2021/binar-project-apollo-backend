const fs = require('fs');
const { VideoMedia } = require('../models');
const { error, success } = require('../utils/response/json.utils');

module.exports = {
  index: async (req, res) => {
    const userId = req.params.id;
    try {
      const videos = await VideoMedia.findAll({ where: { user_id: userId }, order: [['id', 'ASC']] });

      if (videos) {
        return success(res, 201, 'Success Get Data', { videos });
      } else return error(res, 401, 'Error', 'NULL');
    } catch (error) {
      return error(res, 401, Error.message, 'Server Error');
    }
  },
  create: async (req, res) => {
    try {
      const { filename, originalname, path, size } = req.file;
      const { name, description } = req.body;
      const userId = req.params.id;

      const entry = await VideoMedia.create({
        name: filename,
        originalName: originalname,
        description: description,
        videoPath: path,
        size: size,
        user_id: userId,
      });

      return success(res, 201, 'Data Video Create Successfully', entry);
    } catch (Error) {
      return error(res, 401, Error.message, 'Server Error');
    }
  },
  updates: async (req, res) => {
    const { filename, originalname, path, size } = req.file;
    const { name, description } = req.body;
    const paramsId = req.params.id;
    const directoryPath = __basedir + '/public/video/';

    try {
      const dataVideoId = await VideoMedia.findOne({
        where: { id: paramsId },
      });

      if (dataVideoId) {
        const OldFileName = dataVideoId.name;
        fs.unlink(directoryPath + OldFileName, (err) => {
          if (err) {
            res.status(500).send({
              message: 'Could not delete the file. ' + err,
            });
          }
        });
        let newData = {
          name: filename,
          originalName: originalname,
          description: description,
          videoPath: path,
          size: size,
        };
        const dataPhoto = await VideoMedia.update(newData, {
          where: { id: paramsId },
        });
        return success(res, 200, 'Data Changed Successfully', newData);
      } else {
        return error(res, 401, 'Data not found');
      }
    } catch (Error) {
      return error(res, 400, Error.message, 'Server Error');
    }
  },
  deletes: async (req, res) => {
    const paramsId = req.params.id;
    const directoryPath = __basedir + '/public/video/';
    try {
      const dataVideoId = await VideoMedia.findOne({
        where: { id: paramsId },
      });

      if (dataVideoId) {
        const OldFileName = dataVideoId.name;
        fs.unlink(directoryPath + OldFileName, (err) => {
          if (err) {
            res.status(500).send({
              message: 'Could not delete the file. ' + err,
            });
          }
        });
        let erase = await VideoMedia.destroy({ where: { id: paramsId } });
        return success(res, 200, 'Data Deleted Successfully');
      } else {
        return error(res, 401, 'Data not found');
      }
    } catch (Error) {
      return error(res, 400, Error.message, 'Server Error');
    }
  },
};
